import firebase from 'firebase/app';
import firestore from 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyBQ07tkWL6StK6Jy93gqs76fF7RuSpwlUU',
  authDomain: 'ninja-smoothies-683f2.firebaseapp.com',
  databaseURL: 'https://ninja-smoothies-683f2.firebaseio.com',
  projectId: 'ninja-smoothies-683f2',
  storageBucket: 'ninja-smoothies-683f2.appspot.com',
  messagingSenderId: '1068028226724',
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore();
